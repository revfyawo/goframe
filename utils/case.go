package utils

import (
	"strings"
	"unicode"
)

// CamelToSnake converts a CamelCase string to snake_case
func CamelToSnake(camel string) string {
	var builder strings.Builder
	var prev rune
	for _, r := range camel {
		if unicode.IsUpper(r) && unicode.IsLower(prev) {
			builder.WriteRune('_')
		}
		builder.WriteRune(unicode.ToLower(r))
		prev = r
	}
	return builder.String()
}

// SnakeToCamel converts a snake_case string to CamelCase
func SnakeToCamel(snake string) string {
	var builder strings.Builder
	var prev rune
	for _, r := range snake {
		if r != '_' && prev == '_' || prev == 0 {
			builder.WriteRune(unicode.ToUpper(r))
		} else if r != '_' {
			builder.WriteRune(r)
		}
		prev = r
	}
	return builder.String()
}
