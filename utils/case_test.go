package utils

import "testing"

func TestCamelToSnake(t *testing.T) {
	var table = []struct {
		in, out string
	}{
		{"SCREAMING", "screaming"},
		{"ENV_VAR", "env_var"},
		{"CamelCase", "camel_case"},
		{"lowerCamel", "lower_camel"},
		{"ToJSON", "to_json"},
	}

	for _, test := range table {
		if res := CamelToSnake(test.in); res != test.out {
			t.Errorf("wrong case conversion for %s: expected %s got %s",
				test.in, test.out, res)
		}
	}
}

func TestSnakeToCamel(t *testing.T) {
	var table = []struct {
		in, out string
	}{
		{"ENV_VAR", "ENVVAR"},
		{"snake_case", "SnakeCase"},
		{"weird____snake", "WeirdSnake"},
		{"NotSnakeAtAll", "NotSnakeAtAll"},
	}

	for _, test := range table {
		if res := SnakeToCamel(test.in); res != test.out {
			t.Errorf("wrong case conversion for %s: expected %s got %s",
				test.in, test.out, res)
		}
	}
}
