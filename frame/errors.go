package frame

import (
	"fmt"
)

const prefix = "frame error: "

const (
	// DatabaseError is sent when an unknown database
	// error is encountered
	DatabaseError = "database error"

	// WrongDataType is sent when creating a frame for
	// an object that is not a pointer to a structure.
	WrongDataType = "wrong data type"

	// InvalidKeyField is sent when creating a frame for
	// an object with an invalid key field.
	InvalidKeyField = "invalid key field"

	// ObjectExists is sent when inserting a frame for
	// an object that already exists in database.
	ObjectExists = "object exists"

	// ObjectDoesNotExist is sent when fetching an object
	// that does not exist in the databaseà.
	ObjectDoesNotExist = "object does not exist"
)

// Error is a frame error
type Error struct {
	Type    string
	Message string
}

// NewError creates a new error of given type
func NewError(errType, errMsg string) Error {
	return Error{errType, errMsg}
}

func (e Error) Error() string {
	if e.Message == "" {
		return prefix + e.Type
	}
	return fmt.Sprintf("%s: %s", prefix+e.Type, e.Message)
}
