package frame

// Frame is an object stored in a database
type Frame interface {
	// Check checks that the object exists in the database
	Exists() (bool, error)
	// Insert inserts a new object in the database
	Insert() error
	// Load loads an object from the database
	Get() error
	// Save updates an existing object in the database
	Save() error
	// Delete deletes the object from the database
	Delete() error
}
