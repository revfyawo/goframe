package redis

import (
	"reflect"
	"testing"
)

func TestParseStructField(t *testing.T) {
	for name, test := range testTable {
		for field, config := range test.config {
			val, _ := reflect.TypeOf(test.frame.data).Elem().FieldByName(field)
			got := parse(val)
			if config != got {
				t.Errorf("wrong field %s config for %s: expected %+v, got %+v",
					field, name, config, got)
			}
		}
	}
}
