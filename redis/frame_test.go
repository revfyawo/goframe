package redis

import (
	"testing"

	goredis "github.com/go-redis/redis"
	"github.com/stretchr/testify/suite"
)

type FrameTestSuite struct {
	suite.Suite
}

func TestFrame(t *testing.T) {
	suite.Run(t, new(FrameTestSuite))
}

func (suite *FrameTestSuite) SetupSuite() {
	client = goredis.NewClient(&goredis.Options{Addr: "localhost:6379"})
}

func (suite *FrameTestSuite) TearDownSuite() {
	client.Close()
}

func (suite *FrameTestSuite) SetupTest() {
	client.FlushAll()
}

func (suite *FrameTestSuite) TestNewFrame() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testNewFrame(test) })
	}
}

func (suite *FrameTestSuite) TestFrameKey() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testFrameKey(test) })
	}
}

func (suite *FrameTestSuite) TestFrameInsert() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testFrameInsert(test) })
	}
}

func (suite *FrameTestSuite) TestFrameGet() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testFrameGet(test) })
	}
}

func (suite *FrameTestSuite) TestFrameDelete() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testFrameDelete(test) })
	}
}

func (suite *FrameTestSuite) testNewFrame(test tableData) {
	data := test.frame.data
	prefix := test.frame.prefix
	structure := test.frame.structure
	f, err := NewFrame(data, prefix)
	suite.Nil(err, "can't create frame")

	frame := f.(*redisFrame)
	suite.Equalf(prefix, frame.prefix, "wrong key prefix")
	suite.Equalf(data, frame.data, "wrong frame data")
	suite.Equalf(structure, frame.structure, "wrong frame structure")
}

func (suite *FrameTestSuite) testFrameKey(test tableData) {
	f, err := NewFrame(test.frame.data, test.frame.prefix)
	suite.Nil(err, "can't create frame")

	frame := f.(*redisFrame)
	got := frame.key()
	suite.Equal(test.key, got, "wrong redis key")
}

func (suite *FrameTestSuite) testFrameInsert(test tableData) {
	f, err := NewFrame(test.frame.data, test.frame.prefix)
	suite.Require().Nil(err, "can't create frame")

	err = f.Insert()
	suite.Require().Nil(err, "can't insert frame")

	exists, err := client.SIsMember(test.frame.prefix, test.frame.id()).Result()
	suite.Nil(err, "redis error checking frame insertion in the index")
	suite.True(exists, "frame not inserted in the index")

	root, err := client.HGetAll(test.key).Result()
	suite.Require().Nil(err, "redis error checking frame insertion")
	suite.Equal(test.stored.root, root, "wrong inserted frame")

	for field, stored := range test.stored.lists {
		key := test.frame.key() + ":" + field
		list, err := client.LRange(key, 0, int64(len(stored))).Result()
		suite.Nilf(err, "redis error checking frame list %s insertion", key)
		suite.Equal(stored, list, "wrong inserted frame list %s", key)
	}

	for field, stored := range test.stored.hashes {
		key := test.frame.key() + ":" + field
		hash, err := client.HGetAll(key).Result()
		suite.Nil(err, "redis error cheking frame hash %s insertion", key)
		suite.Equal(stored, hash, "wrong inserted frame hash %s", key)
	}
}

func (suite *FrameTestSuite) testFrameGet(test tableData) {
	// Insert data in redis
	err := insertObject(test.key, test.transformed)
	suite.Require().Nil(err, "redis error inserting test data")
	err = insertObjectToIndex(test.frame.prefix, test.key, test.frame.id())

	// Create new frame, and load the fields from redis
	frame, err := NewFrame(test.getData, test.frame.prefix)
	suite.Require().Nil(err, "error creating frame")
	err = frame.Get()
	suite.Nil(err, "error getting frame")
	f := frame.(*redisFrame)
	suite.Equal(test.frame.data, f.data, "wrong data from redis")
}

func (suite *FrameTestSuite) testFrameDelete(test tableData) {
	// Insert data in redis
	err := insertObject(test.key, test.transformed)
	suite.Require().Nil(err, "redis error inserting test data")
	err = insertObjectToIndex(test.frame.prefix, test.key, test.frame.id())

	// Issue delete from new frame
	frame, err := NewFrame(test.getData, test.frame.prefix)
	suite.Require().Nil(err, "error creating frame")
	err = frame.Delete()
	suite.Nil(err, "error deleting frame")
	f := frame.(*redisFrame)

	// Check data is not there anymore
	key := f.key()
	res, err := client.Keys(key + "*").Result()
	suite.Nil(err, "redis error checking key existence after delete")
	suite.Equal([]string{}, res, "unexpected keys still present")

	// Check data is not in the index anymore
	index, err := client.SIsMember(f.prefix, f.id()).Result()
	suite.Nil(err, "redis error checking id deleted from the index")
	suite.False(index, "frame id still in the index")
}
