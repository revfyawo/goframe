package redis

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type TransformTestSuite struct {
	suite.Suite
}

func TestDataToRedis(t *testing.T) {
	suite.Run(t, new(TransformTestSuite))
}

func (suite *TransformTestSuite) TestDataToRedis() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testDataToRedis(test) })
	}
}

func (suite *TransformTestSuite) testDataToRedis(test tableData) {
	frame, err := NewFrame(test.frame.data, test.frame.prefix)
	suite.Require().Nil(err, "can't create frame")

	f := frame.(*redisFrame)
	suite.EqualValues(test.transformed, f.toRedis(), "wrong transformed data")
}
