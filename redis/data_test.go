package redis

type testStringKeyStruct struct {
	Key string `redis:",key"`
	Str string
}

type testIntKeyStruct struct {
	Key int `redis:",key"`
	Str string
}

type testOmitStruct struct {
	Key  string `redis:",key"`
	Str  string
	Omit string `redis:"-"`
}

type testNamedFieldStruct struct {
	Key string `redis:",key"`
	Str string `redis:"mystr"`
}

type testUnexportedFieldStruct struct {
	Key  string `redis:",key"`
	Str  string
	skip string
}

type testIntFieldStruct struct {
	Key string `redis:",key"`
	Int int
}

type testListFieldStruct struct {
	Key  string `redis:",key"`
	List []int
}

type testNestedFieldStruct struct {
	Key    string `redis:",key"`
	Nested struct {
		Int int
	}
}

type testNestedListStruct struct {
	Key        string `redis:",key"`
	NestedRoot struct {
		NestedStr  string
		NestedList []int
	}
}

type testDeepNestedStruct struct {
	Key        string `redis:",key"`
	NestedRoot struct {
		NestedStr  string
		NestedDeep struct {
			NestedDeepInt int
		}
	}
}

type storedData struct {
	root   map[string]string
	lists  map[string][]string
	hashes map[string]map[string]string
}

type tableData struct {
	frame       redisFrame
	getData     interface{}
	key         string
	config      map[string]fieldConfig
	transformed transformed
	stored      storedData
}

var testTable = map[string]tableData{
	"string key test": {
		frame: redisFrame{
			prefix: "testprefix:TestStringKey",
			data:   &testStringKeyStruct{"testkey", "test string"},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Str": {}},
					fromRedisName: map[string]string{"str": "Str"},
					toRedisName:   map[string]string{"Str": "str"},
				},
			},
		},
		getData: &testStringKeyStruct{Key: "testkey"},
		key:     "testprefix:TestStringKey:testkey",
		config: map[string]fieldConfig{
			"Key": {iskey: true},
		},
		transformed: transformed{
			root: map[string]interface{}{"str": "test string"},
		},
		stored: storedData{
			root: map[string]string{"str": "test string"},
		},
	},
	"int key test": {
		frame: redisFrame{
			prefix: "testprefix:TestIntKey",
			data:   &testIntKeyStruct{42, "test string"},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Str": {}},
					fromRedisName: map[string]string{"str": "Str"},
					toRedisName:   map[string]string{"Str": "str"},
				},
			},
		},
		getData: &testIntKeyStruct{Key: 42},
		key:     "testprefix:TestIntKey:42",
		config: map[string]fieldConfig{
			"Key": {iskey: true},
		},
		transformed: transformed{
			root: map[string]interface{}{"str": "test string"},
		},
		stored: storedData{
			root: map[string]string{"str": "test string"},
		},
	},
	"omit field test": {
		frame: redisFrame{
			prefix: "testprefix:TestOmit",
			data:   &testOmitStruct{"testkey", "test string", "omit"},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Str": {}},
					fromRedisName: map[string]string{"str": "Str"},
					toRedisName:   map[string]string{"Str": "str"},
				},
			},
		},
		getData: &testOmitStruct{Key: "testkey", Omit: "omit"},
		key:     "testprefix:TestOmit:testkey",
		config: map[string]fieldConfig{
			"Key":  {iskey: true},
			"Str":  {name: "str"},
			"Omit": {omit: true},
		},
		transformed: transformed{
			root: map[string]interface{}{"str": "test string"},
		},
		stored: storedData{
			root: map[string]string{"str": "test string"},
		},
	},
	"named field test": {
		frame: redisFrame{
			prefix: "testprefix:TestNamedField",
			data:   &testNamedFieldStruct{Key: "testkey", Str: "test string"},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields: map[string]redisField{"Str": {}},
					fromRedisName: map[string]string{
						"mystr": "Str",
					},
					toRedisName: map[string]string{
						"Str": "mystr",
					},
				},
			},
		},
		getData: &testNamedFieldStruct{Key: "testkey"},
		key:     "testprefix:TestNamedField:testkey",
		config: map[string]fieldConfig{
			"Key": {iskey: true},
			"Str": {name: "mystr"},
		},
		transformed: transformed{
			root: map[string]interface{}{"mystr": "test string"},
		},
		stored: storedData{
			root: map[string]string{"mystr": "test string"},
		},
	},
	"unexported field test": {
		frame: redisFrame{
			prefix: "testprefix:TestUnexportedField",
			data:   &testUnexportedFieldStruct{"testkey", "test string", "unexported"},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Str": {}},
					fromRedisName: map[string]string{"str": "Str"},
					toRedisName:   map[string]string{"Str": "str"},
				},
			},
		},
		getData: &testUnexportedFieldStruct{Key: "testkey", skip: "unexported"},
		key:     "testprefix:TestUnexportedField:testkey",
		config: map[string]fieldConfig{
			"Key": {iskey: true},
			"Str": {name: "str"},
		},
		transformed: transformed{
			root: map[string]interface{}{"str": "test string"},
		},
		stored: storedData{
			root: map[string]string{"str": "test string"},
		},
	},
	"int field test": {
		frame: redisFrame{
			prefix: "testprefix:TestIntField",
			data:   &testIntFieldStruct{"testkey", 42},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Int": {}},
					fromRedisName: map[string]string{"int": "Int"},
					toRedisName:   map[string]string{"Int": "int"},
				},
			},
		},
		getData: &testIntFieldStruct{Key: "testkey"},
		key:     "testprefix:TestIntField:testkey",
		config: map[string]fieldConfig{
			"Key": {iskey: true},
			"Int": {name: "int"},
		},
		transformed: transformed{
			root: map[string]interface{}{"int": int64(42)},
		},
		stored: storedData{
			root: map[string]string{"int": "42"},
		},
	},
	"list field test": {
		frame: redisFrame{
			prefix: "testprefix:TestListField",
			data:   &testListFieldStruct{"testkey", []int{3, 1, 4}},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"List": {redisType: redisList}},
					fromRedisName: map[string]string{"list": "List"},
					toRedisName:   map[string]string{"List": "list"},
				},
			},
		},
		getData: &testListFieldStruct{Key: "testkey"},
		key:     "testprefix:TestListField:testkey",
		config: map[string]fieldConfig{
			"Key":  {iskey: true},
			"List": {name: "list", redisField: redisField{redisType: redisList}},
		},
		transformed: transformed{
			lists: map[string][]interface{}{"list": {int64(3), int64(1), int64(4)}},
		},
		stored: storedData{
			root:  map[string]string{},
			lists: map[string][]string{"list": {"3", "1", "4"}},
		},
	},
	"nested field test": {
		frame: redisFrame{
			prefix: "testprefix:TestNestedField",
			data:   &testNestedFieldStruct{"testkey", struct{ Int int }{Int: 42}},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"Nested": {redisType: redisHash}},
					fromRedisName: map[string]string{"nested": "Nested"},
					toRedisName:   map[string]string{"Nested": "nested"},
				},
				nested: map[string]redisHashStruct{
					"Nested": {
						fields:        map[string]redisField{"Int": {}},
						fromRedisName: map[string]string{"int": "Int"},
						toRedisName:   map[string]string{"Int": "int"},
					},
				},
			},
		},
		getData: &testNestedFieldStruct{Key: "testkey"},
		key:     "testprefix:TestNestedField:testkey",
		config: map[string]fieldConfig{
			"Key":    {iskey: true},
			"Nested": {name: "nested", redisField: redisField{redisType: redisHash}},
		},
		transformed: transformed{
			hashes: map[string]map[string]interface{}{"nested": {"int": int64(42)}},
		},
		stored: storedData{
			root:   map[string]string{},
			hashes: map[string]map[string]string{"nested": {"int": "42"}},
		},
	},
	"nested list field test": {
		frame: redisFrame{
			prefix: "testprefix:TestNestedList",
			data: &testNestedListStruct{"testkey", struct {
				NestedStr  string
				NestedList []int
			}{"nested string", []int{3, 1, 4}}},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"NestedRoot": {redisType: redisHash}},
					fromRedisName: map[string]string{"nested_root": "NestedRoot"},
					toRedisName:   map[string]string{"NestedRoot": "nested_root"},
				},
				nested: map[string]redisHashStruct{
					"NestedRoot": {
						fields:        map[string]redisField{"NestedStr": {}, "NestedList": {redisType: redisList}},
						fromRedisName: map[string]string{"nested_str": "NestedStr", "nested_list": "NestedList"},
						toRedisName:   map[string]string{"NestedStr": "nested_str", "NestedList": "nested_list"},
					},
				},
			},
		},
		getData: &testNestedListStruct{Key: "testkey"},
		key:     "testprefix:TestNestedList:testkey",
		config: map[string]fieldConfig{
			"Key":        {iskey: true},
			"NestedRoot": {name: "nested_root", redisField: redisField{redisType: redisHash}},
		},
		transformed: transformed{
			lists: map[string][]interface{}{
				"nested_root.nested_list": {int64(3), int64(1), int64(4)},
			},
			hashes: map[string]map[string]interface{}{
				"nested_root": {"nested_str": "nested string"},
			},
		},
		stored: storedData{
			root: map[string]string{},
			lists: map[string][]string{
				"nested_root.nested_list": {"3", "1", "4"},
			},
			hashes: map[string]map[string]string{
				"nested_root": {"nested_str": "nested string"},
			},
		},
	},
	"deep nested field test": {
		frame: redisFrame{
			prefix: "testprefix:TestDeepNestedField",
			data: &testDeepNestedStruct{
				Key: "testkey",
				NestedRoot: struct {
					NestedStr  string
					NestedDeep struct{ NestedDeepInt int }
				}{
					NestedStr: "nested string",
					NestedDeep: struct {
						NestedDeepInt int
					}{NestedDeepInt: 42},
				},
			},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"NestedRoot": {redisType: redisHash}},
					fromRedisName: map[string]string{"nested_root": "NestedRoot"},
					toRedisName:   map[string]string{"NestedRoot": "nested_root"},
				},
				nested: map[string]redisHashStruct{
					"NestedRoot": {
						fields: map[string]redisField{
							"NestedStr": {}, "NestedDeep": {redisType: redisHash}},
						fromRedisName: map[string]string{
							"nested_str": "NestedStr", "nested_deep": "NestedDeep"},
						toRedisName: map[string]string{
							"NestedStr": "nested_str", "NestedDeep": "nested_deep"},
					},
					"NestedRoot.NestedDeep": {
						fields:        map[string]redisField{"NestedDeepInt": {}},
						fromRedisName: map[string]string{"nested_deep_int": "NestedDeepInt"},
						toRedisName:   map[string]string{"NestedDeepInt": "nested_deep_int"},
					},
				},
			},
		},
		getData: &testDeepNestedStruct{Key: "testkey"},
		key:     "testprefix:TestDeepNestedField:testkey",
		config: map[string]fieldConfig{
			"Key":        {iskey: true},
			"NestedRoot": {name: "nested_root", redisField: redisField{redisType: redisHash}},
		},
		transformed: transformed{
			hashes: map[string]map[string]interface{}{
				"nested_root":             {"nested_str": "nested string"},
				"nested_root.nested_deep": {"nested_deep_int": int64(42)},
			},
		},
		stored: storedData{
			root: map[string]string{},
			hashes: map[string]map[string]string{
				"nested_root":             {"nested_str": "nested string"},
				"nested_root.nested_deep": {"nested_deep_int": "42"},
			},
		},
	},
	"empty nested field test": {
		frame: redisFrame{
			prefix: "testprefix:TestEmptyNestedField",
			data: &testDeepNestedStruct{
				Key: "testkey",
				NestedRoot: struct {
					NestedStr  string
					NestedDeep struct{ NestedDeepInt int }
				}{NestedStr: "nested string"},
			},
			structure: redisStruct{
				keyField: "Key",
				root: redisHashStruct{
					fields:        map[string]redisField{"NestedRoot": {redisType: redisHash}},
					fromRedisName: map[string]string{"nested_root": "NestedRoot"},
					toRedisName:   map[string]string{"NestedRoot": "nested_root"},
				},
				nested: map[string]redisHashStruct{
					"NestedRoot": {
						fields: map[string]redisField{
							"NestedStr": {}, "NestedDeep": {redisType: redisHash}},
						fromRedisName: map[string]string{
							"nested_str": "NestedStr", "nested_deep": "NestedDeep"},
						toRedisName: map[string]string{
							"NestedStr": "nested_str", "NestedDeep": "nested_deep"},
					},
					"NestedRoot.NestedDeep": {
						fields:        map[string]redisField{"NestedDeepInt": {}},
						fromRedisName: map[string]string{"nested_deep_int": "NestedDeepInt"},
						toRedisName:   map[string]string{"NestedDeepInt": "nested_deep_int"},
					},
				},
			},
		},
		getData: &testDeepNestedStruct{Key: "testkey"},
		key:     "testprefix:TestEmptyNestedField:testkey",
		config: map[string]fieldConfig{
			"Key":        {iskey: true},
			"NestedRoot": {name: "nested_root", redisField: redisField{redisType: redisHash}},
		},
		transformed: transformed{
			hashes: map[string]map[string]interface{}{
				"nested_root": {"nested_str": "nested string"},
			},
		},
		stored: storedData{
			root: map[string]string{},
			hashes: map[string]map[string]string{
				"nested_root": {"nested_str": "nested string"},
			},
		},
	},
}
