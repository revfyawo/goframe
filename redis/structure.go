package redis

import (
	"reflect"
	"unicode"
	"unicode/utf8"
)

// global structures map
// the structures are generated the first time calling getStructure
var structures = make(map[string]redisStruct)

type redisStruct struct {
	root     redisHashStruct
	keyField string
	nested   map[string]redisHashStruct
}

type redisHashStruct struct {
	fields        map[string]redisField
	fromRedisName map[string]string
	toRedisName   map[string]string
}

// Adds the given field config into the redis hash srtuct
func (s *redisHashStruct) addField(name string, f fieldConfig) {
	// Lazy structure maps initialization
	if s.fields == nil || s.fromRedisName == nil || s.toRedisName == nil {
		s.fields = make(map[string]redisField)
		s.fromRedisName = make(map[string]string)
		s.toRedisName = make(map[string]string)
	}

	s.fromRedisName[f.name] = name
	s.toRedisName[name] = f.name
	s.fields[name] = f.redisField
}

// Gets the given type structure from global structures, or generate it
func getStructure(dataType reflect.Type) (redisStruct, error) {
	structure, ok := structures[dataType.Name()]
	if ok {
		return structure, nil
	}

	structure = generateStructure(dataType)
	structures[dataType.Name()] = structure
	return structure, nil
}

// generates the given type structure
// dataType should be a structure, otherwise will panic
func generateStructure(dataType reflect.Type) redisStruct {
	structure := &redisStruct{}
	for i := 0; i < dataType.NumField(); i++ {
		f := dataType.Field(i)
		if first, _ := utf8.DecodeRuneInString(f.Name); !unicode.IsUpper(first) {
			continue
		}

		config := parse(f)
		if config.omit {
			continue
		} else if config.iskey {
			structure.keyField = f.Name
			continue
		}

		if config.redisType == redisHash {
			if structure.nested == nil {
				structure.nested = make(map[string]redisHashStruct)
			}
			nested := generateHashStructure(f)
			for hashName, hashStruct := range nested {
				structure.nested[hashName] = hashStruct
			}
		}
		structure.root.addField(f.Name, config)
	}
	return *structure
}

// Generates a given nested field structure, along with its children
// Returns a mapping from field name to field structure
func generateHashStructure(field reflect.StructField) map[string]redisHashStruct {
	structures := make(map[string]redisHashStruct)
	rootStruct := &redisHashStruct{}
	for i := 0; i < field.Type.NumField(); i++ {
		f := field.Type.Field(i)
		if first, _ := utf8.DecodeRuneInString(f.Name); !unicode.IsUpper(first) {
			continue
		}

		config := parse(f)
		if config.redisType == redisHash {
			nested := generateHashStructure(f)
			structures[field.Name+"."+f.Name] = nested[f.Name]
		}
		rootStruct.addField(f.Name, config)
	}
	structures[field.Name] = *rootStruct
	return structures
}
