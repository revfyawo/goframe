package redis

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/revfyawo/goframe/frame"
)

type redisFrame struct {
	data      interface{}
	prefix    string
	structure redisStruct
}

// NewFrame creates a new frame from the data for storing in redis
func NewFrame(data interface{}, prefix string) (frame.Frame, error) {
	// Check data type
	dataType, err := checkDataType(data)
	if err != nil {
		return nil, err
	}

	// Generate structure if not already generated
	structure, err := getStructure(dataType)
	if err != nil {
		return nil, err
	}

	// Check key type
	err = checkKeyType(dataType, structure)
	if err != nil {
		return nil, err
	}

	// Return a new frame
	return &redisFrame{data, prefix, structure}, nil
}

func (f *redisFrame) Exists() (bool, error) {
	// Check the index for the specified id
	exists, err := client.SIsMember(f.prefix, f.id()).Result()
	if err != nil {
		return false, frame.NewError(frame.DatabaseError,
			fmt.Sprintf(`redis error when checking if "%s" exists: %s`,
				f.key(), err))
	}
	return exists, nil
}

func (f *redisFrame) Insert() error {
	// Check key does not exist
	exists, err := f.Exists()
	if err != nil {
		return err
	}
	if exists {
		return frame.NewError(frame.ObjectExists, f.key())
	}

	// Add data to index
	err = insertObjectToIndex(f.prefix, f.key(), f.id())
	if err != nil {
		return err
	}

	// Insert it
	err = insertObject(f.key(), f.toRedis())
	if err != nil {
		return err
	}

	// Everything went well
	return nil
}

func (f *redisFrame) Get() error {
	// Check key does not exist
	exists, err := f.Exists()
	if err != nil {
		return err
	}
	if !exists {
		return frame.NewError(frame.ObjectDoesNotExist, f.key())
	}

	// Get root fields
	root, err := client.HGetAll(f.key()).Result()
	if err != nil {
		return frame.NewError(frame.DatabaseError,
			fmt.Sprintf("error getting %s root fields: %s", f.key(), err))
	}

	// Fill data root fields
	data := reflect.ValueOf(f.data).Elem()
	for name, value := range root {
		fieldName := f.structure.root.fromRedisName[name]
		field := data.FieldByName(fieldName)
		err = fromRedisString(&field, value)
		if err != nil {
			return err
		}
	}

	// Fill data root lists
	for fieldName, fieldConfig := range f.structure.root.fields {
		switch fieldConfig.redisType {
		case redisList:
			key := f.key() + ":" + f.structure.root.toRedisName[fieldName]
			field := data.FieldByName(fieldName)
			err = getList(key, &field)
			if err != nil {
				return err
			}
		}
	}

	// Fill nested lists and deeply nested hashes
	for fieldName, fieldStruct := range f.structure.nested {
		split := strings.Split(fieldName, ".")

		// Get data field
		field := data.FieldByName(split[0])
		prefix := f.key() + ":" + f.structure.root.toRedisName[split[0]]
		for i, fieldName := range split[1:] {
			field = (&field).FieldByName(fieldName)
			parentFieldName := strings.Join(split[:i+1], ".")
			prefix += "." + f.structure.nested[parentFieldName].toRedisName[fieldName]
		}

		// Load from keys
		hash, err := client.HGetAll(prefix).Result()
		if err != nil {
			// TODO: frame.NewError()
			return err
		}

		err = fromRedisHash(&field, hash, f.structure.nested[fieldName])
		if err != nil {
			return err
		}

		// Load nested lists
		for nestedFieldName, nestedFieldConfig := range fieldStruct.fields {
			switch nestedFieldConfig.redisType {
			case redisList:
				key := prefix + "." + fieldStruct.toRedisName[nestedFieldName]
				nestedField := field.FieldByName(nestedFieldName)
				err = getList(key, &nestedField)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (f *redisFrame) Save() error {
	panic("not implemented")
}

func (f *redisFrame) Delete() error {
	// Delete the object id from the index
	err := deleteObjectFromIndex(f.prefix, f.key(), f.id())
	if err != nil {
		return err
	}

	// Store the keys to delete in a slice, to issue only one request
	keys := []string{f.key()}

	// Store lists fields keys
	for fieldName, fieldConfig := range f.structure.root.fields {
		switch fieldConfig.redisType {
		case redisList:
			keys = append(keys, f.key()+":"+f.structure.root.toRedisName[fieldName])
		}
	}

	// Store nested fields keys
	for fieldName, fieldStruct := range f.structure.nested {
		split := strings.Split(fieldName, ".")
		parent := split[0]
		key := f.key() + ":" + f.structure.root.toRedisName[parent]
		for _, child := range split[1:] {
			key += "." + f.structure.nested[parent].toRedisName[child]
			parent += "." + child
		}
		keys = append(keys, key)

		// Store nested lists keys
		for nestedFieldName, nestedFieldConfig := range fieldStruct.fields {
			switch nestedFieldConfig.redisType {
			case redisList:
				keys = append(keys, key+"."+fieldStruct.toRedisName[nestedFieldName])
			}
		}
	}

	// Issue the delete command to delete all object keys
	err = client.Del(keys...).Err()
	if err != nil {
		// TODO: frame.DatabaseError
		return err
	}

	return nil
}

// Returns the value of the key field of a frame
func (f *redisFrame) id() string {
	keyField := reflect.ValueOf(f.data).Elem().FieldByName(f.structure.keyField)
	switch keyField.Type().Kind() {
	case reflect.Int:
		return strconv.Itoa(int(keyField.Int()))
	default:
		return keyField.String()
	}
}

// Returns the redis key where the frame is stored ({prefix}:{id})
func (f *redisFrame) key() string {
	keyBuilder := strings.Builder{}
	keyBuilder.WriteString(f.prefix)
	keyBuilder.WriteRune(':')
	keyBuilder.WriteString(f.id())
	return keyBuilder.String()
}

// Checks that the key field is a string or an int
func checkKeyType(dataType reflect.Type, structure redisStruct) error {
	keyField, ok := dataType.FieldByName(structure.keyField)
	if !ok {
		return frame.NewError(frame.InvalidKeyField,
			fmt.Sprintf("can't find field %s in %s", structure.keyField, dataType.Name()))
	}
	switch keyField.Type.Kind() {
	case reflect.String, reflect.Int:
		return nil
	default:
		return frame.NewError(frame.InvalidKeyField,
			fmt.Sprintf("key field type should be string or int, got %s", keyField.Type.Name()))
	}
}

// Checks that the data passed is a pointer to a struct
func checkDataType(data interface{}) (reflect.Type, error) {
	dataType := reflect.TypeOf(data)
	if dataType.Kind() != reflect.Ptr {
		return nil, frame.NewError(
			frame.WrongDataType,
			fmt.Sprintf("checkDataType expected ptr to struct, got %s", dataType.Kind()),
		)
	}

	dataType = reflect.ValueOf(data).Elem().Type()
	if dataType.Kind() != reflect.Struct {
		return nil, frame.NewError(
			frame.WrongDataType,
			fmt.Sprintf("checkDataType expected ptr to struct, got ptr to %s", dataType.Kind()),
		)
	}

	return dataType, nil
}

// Inserts the given object id in the collection index
func insertObjectToIndex(prefix, key, id string) error {
	err := client.SAdd(prefix, id).Err()
	if err != nil {
		return frame.NewError(frame.DatabaseError,
			fmt.Sprintf(`redis error when adding "%s" to index: %s`,
				key, err))
	}
	return nil
}

// Inserts a transformed object in the collection
func insertObject(key string, data transformed) error {
	if data.root != nil {
		// Insert root keys
		err := client.HMSet(key, data.root).Err()
		if err != nil {
			return frame.NewError(frame.DatabaseError,
				fmt.Sprintf(`redis error when adding "%s" root keys "%#v": %s`,
					key, data.root, err))
		}
	}

	// Insert lists
	for field, data := range data.lists {
		key := key + ":" + field
		err := client.RPush(key, data...).Err()
		if err != nil {
			return frame.NewError(frame.DatabaseError,
				fmt.Sprintf(`redis error when adding "%s" list "%#v": %s`,
					key, data, err))
		}
	}

	// Inser hashes
	for field, data := range data.hashes {
		key := key + ":" + field
		err := client.HMSet(key, data).Err()
		if err != nil {
			return frame.NewError(frame.DatabaseError,
				fmt.Sprintf(`redis error wher adding "%s" hash "%#v": %s`,
					key, data, err))
		}
	}
	return nil
}

// Loads a redis list into the given field
func getList(key string, field *reflect.Value) error {
	length, err := client.LLen(key).Result()
	if err != nil {
		// TODO: frame.NewError()
		return err
	}
	if length == 0 {
		return nil
	}

	list, err := client.LRange(key, 0, length-1).Result()
	if err != nil {
		// TODO: frame.NewError()
		return err
	}

	err = fromRedisList(field, list)
	if err != nil {
		return err
	}
	return nil
}

// Deletes the given object id from the collection index
func deleteObjectFromIndex(prefix, key, id string) error {
	err := client.SRem(prefix, id).Err()
	if err != nil {
		return frame.NewError(frame.DatabaseError,
			fmt.Sprintf(`redis error when removing "%s" from the index: %s\n`,
				key, err))
	}
	return nil
}
