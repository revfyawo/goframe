package redis

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/revfyawo/goframe/utils"
)

type redisType int

const (
	redisString redisType = iota
	redisList
	redisSet
	redisSortedSet
	redisHash
)

type redisField struct {
	redisType redisType
}

type fieldConfig struct {
	redisField
	name  string
	iskey bool
	omit  bool
}

// Parses the field tag
func parse(f reflect.StructField) (config fieldConfig) {
	tag, ok := f.Tag.Lookup("redis")
	if ok {
		flags := strings.Split(tag, ",")
		if len(flags) == 1 {
			switch name := flags[0]; name {
			case "-":
				config.omit = true
				return
			default:
				config.name = name
			}
		} else if flags[0] != "" {
			config.name = flags[0]
		}

		for _, flag := range flags[1:] {
			switch flag {
			case "key":
				config.iskey = true
				return
			}
		}
	}

	fType := f.Type.Kind()
	switch fType {
	case reflect.String, reflect.Int:
		config.redisType = redisString
	case reflect.Array, reflect.Slice:
		config.redisType = redisList
	case reflect.Struct:
		config.redisType = redisHash
	default:
		panic(fmt.Errorf("redis type %s not implemented for field %s", fType.String(), f.Name))
	}

	if config.name == "" {
		config.name = utils.CamelToSnake(f.Name)
	}

	return
}
