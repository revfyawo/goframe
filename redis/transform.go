package redis

import (
	"reflect"
	"strconv"
)

type transformed struct {
	root   map[string]interface{}
	lists  map[string][]interface{}
	hashes map[string]map[string]interface{}
}

// Transform a frame into a suitable format for redis insertion
func (f *redisFrame) toRedis() transformed {
	var ret = transformed{}

	data := reflect.ValueOf(f.data).Elem()
	for fieldName, redisName := range f.structure.root.toRedisName {
		redisType := f.structure.root.fields[fieldName].redisType
		switch redisType {
		case redisString:
			data, zero := getRedisString(data.FieldByName(fieldName))
			if zero {
				continue
			}

			if ret.root == nil {
				ret.root = make(map[string]interface{})
			}
			ret.root[redisName] = data
		case redisList:
			data, zero := getRedisList(data.FieldByName(fieldName))
			if zero {
				continue
			}

			if ret.lists == nil {
				ret.lists = make(map[string][]interface{})
			}
			ret.lists[redisName] = data
		case redisHash:
			transformedHash := getRedisHash(fieldName, redisName, data.FieldByName(fieldName), f.structure)

			for field, value := range transformedHash.root {
				if ret.hashes == nil {
					ret.hashes = make(map[string]map[string]interface{})
				}
				if ret.hashes[redisName] == nil {
					ret.hashes[redisName] = make(map[string]interface{})
				}
				ret.hashes[redisName][field] = value
			}
			for list, value := range transformedHash.lists {
				if ret.lists == nil {
					ret.lists = make(map[string][]interface{})
				}
				ret.lists[redisName+"."+list] = value
			}
			for hash, value := range transformedHash.hashes {
				if ret.hashes == nil {
					ret.hashes = make(map[string]map[string]interface{})
				}
				ret.hashes[redisName+"."+hash] = value
			}
		}
	}

	return ret
}

// Returns a string from given data (string or int) for insertion into redis
func getRedisString(data reflect.Value) (str interface{}, zero bool) {
	switch data.Type().Kind() {
	case reflect.String:
		str = data.String()
		return str, str == ""
	case reflect.Int:
		str = data.Int()
		return str, str == int64(0)
	}
	return nil, false
}

// Returns a list from given data for insertion into redis
func getRedisList(data reflect.Value) (list []interface{}, zero bool) {
	if data.IsNil() || data.Len() == 0 {
		return nil, true
	}

	for i := 0; i < data.Len(); i++ {
		value, _ := getRedisString(data.Index(i))
		list = append(list, value)
	}
	return list, false
}

// Returns a hash from given data and structure for insertion into redis
// Children fields are separated with a "." character
func getRedisHash(rootFieldName, rootRedisName string, data reflect.Value, structure redisStruct) transformed {
	ret := transformed{}
	hashStruct := structure.nested[rootFieldName]
	for fieldName, redisName := range hashStruct.toRedisName {
		field := data.FieldByName(fieldName)
		redisType := hashStruct.fields[fieldName].redisType
		switch redisType {
		case redisString:
			value, zero := getRedisString(field)
			if zero {
				continue
			}
			if ret.root == nil {
				ret.root = make(map[string]interface{})
			}
			ret.root[redisName] = value
		case redisList:
			value, zero := getRedisList(field)
			if zero {
				continue
			}
			if ret.lists == nil {
				ret.lists = make(map[string][]interface{})
			}
			ret.lists[redisName] = value
		case redisHash:
			hashFieldName := rootFieldName + "." + fieldName
			hashRedisName := rootRedisName + "." + redisName
			transformedHash := getRedisHash(hashFieldName, hashRedisName, field, structure)

			for field, value := range transformedHash.root {
				if ret.hashes == nil {
					ret.hashes = make(map[string]map[string]interface{})
				}
				if ret.hashes[redisName] == nil {
					ret.hashes[redisName] = make(map[string]interface{})
				}
				ret.hashes[redisName][field] = value
			}
			for list, value := range transformedHash.lists {
				if ret.lists == nil {
					ret.lists = make(map[string][]interface{})
				}
				ret.lists[redisName+"."+list] = value
			}
			for hash, value := range transformedHash.hashes {
				if ret.hashes == nil {
					ret.hashes = make(map[string]map[string]interface{})
				}
				ret.hashes[redisName+"."+hash] = value
			}
		}
	}
	return ret
}

// Fills given string or int field with string data from redis
func fromRedisString(field *reflect.Value, str string) error {
	switch field.Type().Kind() {
	case reflect.String:
		field.SetString(str)
	case reflect.Int:
		intval, err := strconv.Atoi(str)
		if err != nil {
			// TODO: frame.NewError()
			return err
		}
		field.SetInt(int64(intval))
	}
	return nil
}

// Fills given list field with list data from redis
func fromRedisList(field *reflect.Value, list []string) error {
	fieldType := field.Type()
	elemKind := fieldType.Elem().Kind()
	length := len(list)
	slice := reflect.MakeSlice(fieldType, length, length)
	for i := 0; i < length; i++ {
		switch elemKind {
		case reflect.String:
			slice.Index(i).SetString(list[i])
		case reflect.Int:
			val, err := strconv.Atoi(list[i])
			if err != nil {
				// TODO: frame.NewError()
				return err
			}
			slice.Index(i).SetInt(int64(val))
		}
	}
	field.Set(slice)
	return nil
}

// Fills given struct field with hash data from redis
func fromRedisHash(field *reflect.Value, hash map[string]string, structure redisHashStruct) error {
	fieldType := field.Type()
	if !field.IsValid() {
		field.Set(reflect.New(fieldType).Elem())
	}

	for hashKey, hashVal := range hash {
		hashField := field.FieldByName(structure.fromRedisName[hashKey])
		switch hashField.Type().Kind() {
		case reflect.String:
			hashField.SetString(hashVal)
		case reflect.Int:
			val, err := strconv.Atoi(hashVal)
			if err != nil {
				// TODO: frame.NewError()
				return err
			}
			hashField.SetInt(int64(val))
		}
	}
	return nil
}
