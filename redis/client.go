package redis

import (
	goredis "github.com/go-redis/redis"
)

var client *goredis.Client

// Connect connects redis frames to a redis server
func Connect(opts *goredis.Options) error {
	client = goredis.NewClient(opts)
	return client.Ping().Err()
}
