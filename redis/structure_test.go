package redis

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/suite"
)

type StructureTestSuite struct {
	suite.Suite
}

func TestStructure(t *testing.T) {
	suite.Run(t, new(StructureTestSuite))
}

func (suite *StructureTestSuite) TestGenerateStructure() {
	for name, test := range testTable {
		suite.Run(name, func() { suite.testGenerateStructure(test) })
	}
}

func (suite *StructureTestSuite) testGenerateStructure(test tableData) {
	got := generateStructure(reflect.ValueOf(test.frame.data).Elem().Type())
	expected := test.frame.structure
	suite.Equal(expected.keyField, got.keyField, "wrong key field")
	suite.Equal(expected.root.fromRedisName, got.root.fromRedisName, "wrong redis to struct field name map")
	suite.Equal(expected.root.toRedisName, got.root.toRedisName, "wrong struct to redis field name map")
	suite.Equal(expected.root.fields, got.root.fields, "wrong redis fields")
	suite.Equal(expected.nested, got.nested, "wrong redis nested fields")
}
